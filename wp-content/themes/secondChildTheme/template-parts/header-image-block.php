<?php if(get_row_layout() == 'header_image_block'):?>
  <div class="header-image-block">
    <img class="hib-image" src="<?php the_sub_field('image') ?>" alt="">
    <div class="hib-center-content">
      <p class="hib-cc-cn"><?php the_sub_field('client_name') ?></p>
      <p class="hib-cc-pn"><?php the_sub_field('project_name') ?></p>
    </div>
    <p class="hib-pt"><?php the_sub_field('project_tags') ?></p>
  </div>
<?php endif; ?>

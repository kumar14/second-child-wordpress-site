<?php if(get_row_layout() == 'text_and_image_block'):?>
  <div class="text_and_image_block" style="background-color:<?php the_sub_field('background_color') ?> ;">

    <div class="taib-content-field">
      <h3 class="taib-headline"><?php the_sub_field('headline'); ?></h3>
      <div class="taib-content">
        <?php the_sub_field('content') ?>
      </div>
    </div>

    <img class="taib-image" src="<?php the_sub_field('image') ?>" alt="">
  </div>
<?php endif; ?>

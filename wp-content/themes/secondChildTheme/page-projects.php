<?php get_header();  ?>
  <main class="container page section classes-index">
    <div class="inline-menu-collapsed" reverse-main-menu>
        <h1 class="inline-menu-title">Featured</h1>
        <ul class="inline-menu-selections">
          <li id="film">Film</li>
          <li id="photography">Photography</li>
          <li id="retouching">Retouching</li>
          <li id="print">Print</li>
          <li id="development">Development</li>
          <li id="all">All</li>
        </ul>
    </div>
    <?php
      secondchild_projects_list();
    ?>
  </main>
<?php get_footer(); ?>

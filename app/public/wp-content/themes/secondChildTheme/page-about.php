<body class="about-page" >
  <?php get_header();  ?>


    <!-- <video autoplay controls>
      <source src="<?php the_field('identifier'); ?>" type="video/">
    </video> -->
    <!-- <video autoplay controls>
      <source src="https://vimeo.com/117425736" type="video/">
    </video> -->

<!-- <video autoplay="autoplay" playsinline="" muted="muted" loop="loop" preload="" width="100%" controlslist="nodownload"><source src="../wp-content/uploads/2020/08/2020-Reel-Work-compressed.mp4"></video> -->

<div class="video-player" style="visibility: inherit; opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);">
  <div class="video-player-button">
    <div class="video-player-button-label">Play with audio</div>
    <div class="video-player-button-background"></div>
  </div>
  <video autoplay="autoplay" playsinline="" muted="muted" loop="loop" preload="" width="100%" controlslist="nodownload">
    <source src="../wp-content/uploads/2020/08/2020-Reel-Work-compressed.mp4">
  </video>
</div>

    <section class="who-we-are-about page-width">
      <p>about us</p>
      <h2>Second Child is...</h2>
      <h3>a forward-thinking, integrted production studio in the heart of downtown New York City.</h3>
      <h3>With a small, tight-knit team and an adaptable work style, we create real things in real time across the disciplines of film, animation, photography, graphics and technology.</h3>
      <h4>What we offer</h4>
      <div class="offers-list">
        <p>Film Production</p>
        <p>Photography</p>
        <p>App</p>
        <p>Animation</p>
        <p>Print Production</p>
        <p>Development</p>
        <p>Editorial</p>
        <p>Retouching</p>
        <p>VR + AR</p>
        <p>Motion Graphics</p>
        <p>Fabrication</p>
        <p>Stages</p>
        <p>Audio Record + Mix</p>
        <p>Interactive Design</p>
        <p>Green Rooms</p>
        <p>Sound Design</p>
        <p>Interactive Development</p>

      </div>
    </section>

    <div class="line-break"></div>

    <h2 class="who-we-are page-width">Who We Are</h2>
    <?php secondchild_people_list() ?>

</body>

<?php get_footer(); ?>

<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */

define( 'WP_HOME', 'http://second-child.local' );
define( 'WP_SITEURL', 'http://second-child.local' );


define( 'DB_NAME', 'wordpress');

/** MySQL database username */
define( 'DB_USER', 'wordpress');

/** MySQL database password */
define( 'DB_PASSWORD', 'wordpress');

/** MySQL hostname */
define( 'DB_HOST', 'db:3306');

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '');

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'G+HLIcBLilmWN+0aJEQ+CXrxS7B0m3VAaZ8FOULIjfLJi2/AwUlgwoNvVOXSGBtgJ04z6T4ZtdE0dKYctQnXzA==');
define('SECURE_AUTH_KEY',  'iOwzfcROHPrSqrw//HdUB+4dpPu4s7UKP9jvjfsDieR5RlLjVpuM2H3zDbHjWZ9SLcJBC2FUdjfNeqh9M21yUA==');
define('LOGGED_IN_KEY',    '0JrCCIMX8gkuy1z9H70F3yQMIWdpT506L1LzPHHqzUpVXz9EJh7yTFuQgzA2wpE7WoezvpnwgvpPMQ4R2d6BeQ==');
define('NONCE_KEY',        '1xiP0LDegqf83UKr1QEkWlltvVX5s9xNRmQ2LuMHwBjtsbVp7nyH6GkJukroa0vZqzooBX2u8zo4F3SejccU+w==');
define('AUTH_SALT',        'F5ECgutKnInZyd/baH8tzQASVqEtNnK1chrWQEX4CIMBAfWjMyz12B84XT/1sl+V9hyv+Dxy+dzpjJPoGNgoag==');
define('SECURE_AUTH_SALT', 'qypp44InbVHM1FrwGFy0s8d26SEh4wbbCIGctBo2qHGiKOYIAF2EhKkpKl5kex3ep8v324pSkBhvuSK1BA9SKA==');
define('LOGGED_IN_SALT',   '1zQV4mvB8q1KvKoelc1wXNWDS16ivtnxyfTCwfSmE3kj6sP+kvwRdwU9krV4vXKmka3lCmpliYLEM6YEWRcrnw==');
define('NONCE_SALT',       'NqKB8MHAI8PrkQ2s3wv3a9P7BtCQnpB+IdHmEqx/FYd/oBRwlkBbDJ6EgjwR0OAO7Zd4JIqp7uGaP1BaJeUiew==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';

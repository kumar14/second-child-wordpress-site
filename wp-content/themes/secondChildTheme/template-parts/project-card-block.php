
<?php
  $card_class = ($project_card_type == 'full') ? 'featured-project' : '';
  $is_anchor_wrap = !($project_card_type == 'full');
  $href = get_permalink();
?>
<div class="main-project-card <?php echo $card_class ?>">
<?php if ($is_anchor_wrap) { echo "<a class='card-wrapper-cta' href='{$href}'> "; } ?>

<?php the_post_thumbnail($project_card_type)?>
  <div class="card-content">

    <div class="inner-content">
      <h5 class="client-name"><?php the_field('client_name'); ?></h5>
      <h3><?php the_title(); ?></h3>
    </div>
    <?php
      $tag_list = get_field('project_tags');
      $tag_list = str_replace(', ',',',$tag_list);
      $tags = explode(',',$tag_list);
      echo '<ul class="project-tags">';
      foreach ( $tags as $tag ) :
        echo "<li>$tag</li>";
      endforeach;
      echo '</ul>';
    ?>
  </div>

  <?php if ($is_anchor_wrap){ echo "</a>";} ?>
</div>

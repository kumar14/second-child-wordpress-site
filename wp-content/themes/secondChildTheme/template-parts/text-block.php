<?php if(get_row_layout() == 'text_block'):?>
  <div class="text-block" style="background-color:<?php the_sub_field('background_color') ?> ; padding-top: <?php the_sub_field('padding_top') ?>px; padding-bottom: <?php the_sub_field('padding_bottom') ?>px;">
    <h3 class="tb-headline"><?php the_sub_field('headline') ?></h3>
    <p class="tb-client"><?php the_sub_field('client_name') ?></p>
    <p class="tb-project"><?php the_sub_field('project_name') ?></p>
    <div class="tb-content">
      <?php the_sub_field('content') ?>
    </div>
  </div>
<?php endif; ?>

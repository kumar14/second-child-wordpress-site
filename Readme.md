# SecondChild site

As of right now to get the site running on my machine is using Local by flywheel and a free version of MAMP which is a sql server.

[Local by flywheel](https://localwp.com/)
[Mamp](https://www.mamp.info/en/mac/)

For local by flywheel, you would just create a site (Second Child in this instance) and you'll have a faux wordpress local on your machine with a login. Under your site name will be where the file is located and an arrow. This is where the files are on your machine. I would click on that arrow, and basically drag and drop the gitlab repo.

Im not 100% how to dump the sql, and after we get it rolling, feedback on setup for a local environment would be greatly appreciated and needed.

## WP Engine and SFTP
There are currently three environments for the 2C site (production, staging, dev).

The dev url and credentials are:

final url: https://dev.secondchild.nyc
working url: http://secondchilddev.wpengine.com/


WP login url: https://dev.secondchild.nyc/wp-login.php
u: secondchilddev
p: cow horse banana boom
(please copy + paste, the password does have spaces in it)

I will create a few credentials for the WP engine login. To add my changes onto the dev site on wp engine, I deploy my changes via an sftp. If needed I will create an sftp login or just share mine.

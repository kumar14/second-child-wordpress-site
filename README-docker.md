# Docker development environment
Setup and run a docker wordpress instance on http://second-child.local/

http://second-child.local/  
http://second-child.local/wp-admin/  

## Requirements
- homebrew https://brew.sh
- docker https://docs.docker.com/get-docker/
- docker-machine https://docs.docker.com/machine/install-machine/
- docker-compose https://docs.docker.com/compose/install/
- A running docker machine instance

## Install Homebrew
If you don't yet have Homebrew installed, you'll need to install it first.

    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

## Install Docker, Docker-Machine, Docker-Compose using Homebrew

    brew install docker docker-machine docker-compose
    brew cask install virtualbox

## Create the local docker-machine

    docker-machine create --driver virtualbox --virtualbox-cpu-count=2 --virtualbox-memory=4096 default
    docker-machine env default
    eval $(docker-machine env default)
    # to avoid having to manually run the above command to work with your docker environment, add it to your ~.zshrc (if still using Bash ~/.bash_profile or ~/.profile)

    # you can verify any running docker machines using
    docker-machine ls

## Setup host ip for second-child.local
In this example my docker machine ip is: 192.168.99.111
  ```
  docker-machine ip

  192.168.99.111
  ```

Add to /etc/hosts
  ```
  192.168.99.111 second-child.local
  ```

Flush the dns cache
  ```
  sudo dscacheutil -flushcache
  ```

## Bootstrap the installation
Start the instance with compose (run this command from the root of the repository where the docker-compose.yml file exists)
  ```
  docker-compose up
  ```

Find the container name
  ```
  docker ps

  CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                 NAMES
  de4382dfb08b        wordpress:latest    "docker-entrypoint.s…"   About an hour ago   Up About an hour    0.0.0.0:80->80/tcp    second-child-wordpress-site_wordpress_1
  1a82706ce1c3        mysql:5.7           "docker-entrypoint.s…"   About an hour ago   Up About an hour    3306/tcp, 33060/tcp   second-child-wordpress-site_db_1
  ```

Assign it for convenience
  ```
  DOCK_DB=second-child-wordpress-site_db_1
  ```

Import a sql dump from phpmyadmin on `development`  
  - Export sql dump: https://wpengine.com/resources/how-to-backup-wordpress/#Back_Up_the_WordPress_Database_Using_phpMyAdmin
  - phpMyAdmin: https://my.wpengine.com/installs/secondchilddev/phpmyadmin
  
  ```
  docker exec -i $DOCK_DB mysql -uroot -pwordpress wordpress < dump.sql
  ```

Recreate the database to import new sql dumps
  ```
  docker exec -it $DOCK_DB mysqladmin -uroot -pwordpress drop wordpress
  docker exec -it $DOCK_DB mysqladmin -uroot -pwordpress create wordpress
  ```

Reset the admin to user: wordpress, password: wordpress
  ```
  docker exec -it $DOCK_DB mysql -uroot -pwordpress -e "use wordpress; UPDATE wp_users SET user_pass = MD5('wordpress'), user_login = 'wordpress', user_email = 'admin@wp.tld', display_name = 'admin' WHERE ID=1;"
  ```

Access the site at http://second-child.local/

Stop the instance
  ```
  docker-compose down
  ```

## Download the uploads folder from the dev environment
Download the latest changes to wp-content/uploads folder from dev. In this example the user `secondchilddev-dev` was created on wpengine. This will overwrite existing files in the folder. ***Do not commit the uploads folder.*** Upload content should be made on the dev environment and then pulled down locally.

    scp -P2222 -r secondchilddev-dev@secondchilddev.sftp.wpengine.com:wp-content/uploads/* ./wp-content/uploads

## Editing the docker config
See docker-compose.yml and wp-config.docker.php

### Changing http ports
If the default port 80 is already used, add these defines to docker/wp-config.php
  ```
  define( 'WP_HOME', 'http://second-child.local:8000' );
  define( 'WP_SITEURL', 'http://second-child.local:8000' );
  ```

Change the service expose port in docker-compose.yml
  ```
  wordpress:
    ports:
      - "8000:80"
  ```

Access the site at http://second-child.local:8000/


## Deploy with GIT to Second Child's dev environment on WPEngine
Review the docs https://wpengine.com/support/git/ for setting up your user and remote and other best practices. We only deploy to dev and will use migrations to push to staging and prod.  

Set the remote to `secondchilddev` as `development`  

    git remote add development git@git.wpengine.com:production/secondchilddev.git

Commit changes to `master`. Only push from this branch to keep in order.  

    git checkout master

Once your WPEngine git ssh user is configured and available, push the committed changes. (In the case of this project the git deployment branch on the development remote was named `develop`.)

    git push development HEAD:develop

## Helpful Info

### List running Docker machines
    docker-machine ls

### Start / stop the Docker machine
    docker-machine start default
    docker-machine stop default

### List existing containers

    docker-compose ps

### Get a bash shell in a container

    # to get the container name, use the docker ps command
    docker exec -it <container name> /bin/bash

    # examples
    docker exec -it compose_author_1 /bin/bash
    docker exec -it compose_publish_1 /bin/bash
    docker exec -it compose_dispatcher_1 /bin/bash

### Deleting the Docker machine
If you would like to completely remove your Docker Machine and start over, use the following:

    # NOTE: this will delete your machine and all setup commands will need to be run again
    docker-machine rm default

### Completely purging Docker
Docker provides a single command that will clean up any resources — images, containers, volumes, and networks — that are dangling (not associated with a container).

    # remove all unused containers, networks, images (only dangling)
    docker system prune

    # remove any stopped containers and all unreferenced images (not just dangling images)
    docker system prune -a

    # also remove volumes
    docker system prune -a --volumes



## Using Docker VME
Replicating the droga5 site and it's initial setup, if you are using the docker-vme, the steps are roughly the same.

### Initializing Docker and Setup
1. Before anything, please check if you have anything previous docker machines running in the background by running
```
docker-machine ls
```
If so, run and run the first command to make sure no machine is running:
```
docker-machine down
```
2. If you're using virtualbox, have that running and simply run the command:
```
docker-machine start docker-vme
```
or
```
docker-machine start
```
3. This will start the machine. By adding docker-vme, you are stating where it is being Initializing
Afterwards please check if you have docker running
```
docker-machine ls
```
4. There should be docker-vme, afterwards we are going to add the env by running
```
docker-machine env docker-vme
```
5. You will get a list of commands to configure the shell, in this case is
```
eval $(docker-machine env docker-vme)
```
6. The next step is adding your ip to your etc/hosts file. You're adding this:   (note, if you're having trouble getting to your etc/hosts in a new tab in terminal, hit cd, and then whatever editor you're using for me it is atom so atom. etc/hosts/)
```
192.168.99.111 second-child.local
```
In this instance that is my ip being used, but for each person it will be different, afterwards run this command.
```
docker-machine ip docker-vme
```
7. Afterwards you are then flushing the DNS cache by running this command
```
sudo dscacheutil -flushcache
```
8. To get docker running just run:
```
docker-compose up
```
to turn off the machine run
```
docker-machine downtown
```
9. Lastly, if you need to open a new terminal command but are still working in the same repo, make sure to do steps 4 and 5 again, so the env remains the same.
10. After step 6 you can pretty much follow the instructions on top.

## Migrating environments with WP Engine
Follow the directions in https://wpengine.com/support/wp-engine-automatic-migration-powered-by-blogvault/

Start the migration from the `development` environment  
https://my.wpengine.com/installs/secondchilddev/migrate-my-site

The *WP Engine Automated Migration* plugin should already be activated. The credentials for the *Is Your Site Password Protected* section are http auth creds.

<body class="about-page" >
  <?php get_header();  ?>

  <?php $autoplay = get_field('autoplay') ? 'autoplay' : ''; ?>
  <div class="video-player" style="visibility: inherit; opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0); padding-top: <?php the_field('padding_top'); ?>px; padding-bottom: <?php the_field('padding_bottom'); ?>px;">
    <div class="video-custom-button">
      <div class="video-custom-button-label">Enable audio</div>
      <div class="video-custom-button-background"></div>
    </div>
    <div class="video-cta-play" style="display:none;"></div>
    <video <?php echo $autoplay; ?> playsinline="" muted="muted" loop="loop" preload="" width="100%" controlslist="nodownload" controls-on-click>
      <source src="<?php the_field('video-src'); ?>">
    </video>
  </div>

    <section class="who-we-are-about page-width">
      <h1 class="page-tag">about us</h1>
      <h2>Second Child is...</h2>
      <h3>a forward-thinking, integrated production studio in the heart of downtown New York City.</h3>
      <h3>With a small, tight-knit team and an adaptable work style, we create real things in real time across the disciplines of film, animation, photography, graphics and technology.</h3>
      <h4 class="offers-header">What We Offer</h4>
      <div class="offers-list">
        <p>Film Production</p>
        <p>Photography</p>
        <p>App</p>
        <p>Animation</p>
        <p>Print Production</p>
        <p>Development</p>
        <p>Editorial</p>
        <p>Retouching</p>
        <p>VR + AR</p>
        <p>Motion Graphics</p>
        <p>Fabrication</p>
        <p>Stages</p>
        <p>Audio Record + Mix</p>
        <p>Interactive Design</p>
        <p>Green Rooms</p>
        <p>Sound Design</p>
        <p>Interactive Development</p>

      </div>
    </section>

    <div class="line-break"></div>

    <h2 class="who-we-are page-width">Who We Are</h2>
    <?php secondchild_people_list() ?>

</body>

<?php get_footer(); ?>

<?php get_header(); ?>

<?php while(have_posts() ): the_post(); ?>
  <?php $project_card_type = 'full'; ?>
  <?php include(locate_template('template-parts/project-card-block.php')); ?>
  <?php the_content(); ?>
  <?php if(have_rows('content_layers')): ?>

    <?php while(have_rows('content_layers')): the_row(); ?>

      <?php get_template_part('template-parts/quote-block');  ?>
      <?php get_template_part('template-parts/text-block');  ?>
      <?php get_template_part('template-parts/custom-html');  ?>
      <?php get_template_part('template-parts/text-and-image-block');  ?>
      <?php get_template_part('template-parts/homepage-block-fullscreen');  ?>
      <?php get_template_part('template-parts/two-column-grid');  ?>
      <?php get_template_part('template-parts/image-block');  ?>
      <?php get_template_part('template-parts/logo-grid');  ?>
      <?php get_template_part('template-parts/image-carousel');  ?>
      <?php get_template_part('template-parts/page-background-color');  ?>
      <?php get_template_part('template-parts/header-image-block');  ?>
      <?php get_template_part('template-parts/three-image-collage');  ?>

    <?php endwhile ?>
  <?php endif ?>

<?php endwhile; ?>




<?php get_footer(); ?>

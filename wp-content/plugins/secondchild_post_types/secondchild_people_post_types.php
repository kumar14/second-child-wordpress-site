<?php
  /*
    Plugin Name: SecondChild - People Post Types
    Plugin URI:
    Description: Adds a new post type into Wordpress
    Version: 1.0
    Author: SecondChild
    Author URI: https://secondchild.nyc
    Text Domain: SecondChild
  */

  if(!defined('ABSPATH')) die();


// Register new Custom Post Type
function secondchild_class_people_post_type() {

	$labels = array(
		'name'                  => _x( 'People', 'Post Type General Name', 'secondchild' ),
		'singular_name'         => _x( 'Person', 'Post Type Singular Name', 'secondchild' ),
		'menu_name'             => __( 'People', 'secondchild' ),
		'name_admin_bar'        => __( 'People', 'secondchild' ),
		'archives'              => __( 'Archive', 'secondchild' ),
		'attributes'            => __( 'Attributes', 'secondchild' ),
		'parent_item_colon'     => __( 'Parent Item Colon', 'secondchild' ),
		'all_items'             => __( 'All People', 'secondchild' ),
		'add_new_item'          => __( 'Add Person', 'secondchild' ),
		'add_new'               => __( 'Add new', 'secondchild' ),
		'new_item'              => __( 'New Person', 'secondchild' ),
		'edit_item'             => __( 'Edit Person', 'secondchild' ),
		'update_item'           => __( 'Update Person', 'secondchild' ),
		'view_item'             => __( 'View Person', 'secondchild' ),
		'view_items'            => __( 'View Person', 'secondchild' ),
		'search_items'          => __( 'Search Person', 'secondchild' ),
		'not_found'             => __( 'Not found', 'secondchild' ),
		'not_found_in_trash'    => __( 'Not found in trash', 'secondchild' ),
		'featured_image'        => __( 'Featured Image', 'secondchild' ),
		'set_featured_image'    => __( 'Save Featured Image', 'secondchild' ),
		'remove_featured_image' => __( 'Remove Featured Image', 'secondchild' ),
		'use_featured_image'    => __( 'Use as Featured Image', 'secondchild' ),
		'insert_into_item'      => __( 'Insert in Person', 'secondchild' ),
		'uploaded_to_this_item' => __( 'Add in Person', 'secondchild' ),
		'items_list'            => __( 'People List', 'secondchild' ),
		'items_list_navigation' => __( 'Navigate to People', 'secondchild' ),
		'filter_items_list'     => __( 'Filter People', 'secondchild' ),
	);
	$args = array(
		'label'                 => __( 'Person', 'secondchild' ),
		'description'           => __( 'People for SecondChild Website', 'secondchild' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'hierarchical'          => false, // False = posts - No child posts
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
    'menu_position'         => 8,
    'menu_icon'             => 'dashicons-welcome-learn-more',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page'
    // 'show_in_rest'          => true
	);
	register_post_type( 'people', $args );
}
add_action( 'init', 'secondchild_class_people_post_type', 0);
?>

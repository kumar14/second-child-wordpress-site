<?php if(get_row_layout() == 'two_column_grid'):?>
  <div class="two_column_grid" style="background-color:<?php the_sub_field('background_color') ?> ;">
    <div class="column1">
      <?php while(have_rows('column1')): the_row(); ?>
          <h3><?php the_sub_field('headline') ?></h3>

          <div class="column1-content">
            <?php the_sub_field('content') ?>
          </div>

          <img src="<?php the_sub_field('image') ?>" />
          <p><?php the_sub_field('caption') ?></p>
      <?php endwhile ?>
    </div>

    <div class="column2">
      <?php while(have_rows('column2')): the_row(); ?>
        <h3><?php the_sub_field('headline') ?></h3>

        <div class="column2-content">
          <?php the_sub_field('content') ?>
        </div>

        <img src="<?php the_sub_field('image') ?>" />
        <p><?php the_sub_field('caption') ?></p>
      <?php endwhile ?>
    </div>
  </div>
<?php endif; ?>

<?php if(get_row_layout() == 'quote_block'):?>
  <div class="quote-block center-module" style="background-color:<?php the_sub_field('background_color') ?> ;">
    <img class="quote-image" src="<?php the_sub_field('person_image') ?>" alt="">
    <div class="quote-content">
      <i class="quote-text">"<?php the_sub_field('quote_text')?>"</i>
      <p class="quote-person-name">-<?php the_sub_field('person_name') ?></p>
      <p class="quote-person-title"><?php the_sub_field('person_title') ?></p>
    </div>

  </div>
<?php endif; ?>

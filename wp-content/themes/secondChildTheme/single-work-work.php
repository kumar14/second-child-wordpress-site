<?php get_header(); ?>

<?php while(have_posts() ): the_post(); ?>
  <h1><?php the_title(); ?></h1>
  <?php the_content(); ?>
<?php endwhile; ?>

<?php if(get_field('text_block')): ?>
  <?php the_field('text_block'); ?>
<?php endif; ?>

<h1>TESTING</h1>

<?php get_footer(); ?>

<footer class="site-footer">
  <div class="footer-content">
    <div class="footer-left">
      <span>© <?php echo date('Y') ?> Second Child</span>
    </div>
    <div class="footer-right">
      <span>
        <a href="<!?php echo instagram_url(); ?>">
            <?php echo inline_instagram_logo(); ?>
          </a>
      </span>
      <span>
        <a href="<!?php echo linkedin_url(); ?>">
            <?php echo inline_linkedin_logo(); ?>
          </a>
      </span>
    </div>
  </div>
</footer>

  </body>
</html>

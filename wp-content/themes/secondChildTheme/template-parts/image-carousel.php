<?php if(get_row_layout() == 'image_carousel'):?>
  <div class="image-carousel" style="background-color:<?php the_sub_field('background_color') ?> ;">
      <?php while(have_rows('images')): the_row(); ?>
        <div class="image-carousel-container">
          <img src="<?php the_sub_field('image') ?>" />
          <p><?php the_sub_field('caption') ?></p>
        </div>
      <?php endwhile ?>
  </div>
<?php endif; ?>

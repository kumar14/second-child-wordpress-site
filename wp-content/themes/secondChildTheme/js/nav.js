jQuery(document).ready(function($) {
  $(".menu-collapsed").click(function() {
    $(this).toggleClass("menu-expanded");
  });
  $(".inline-menu-collapsed").on('click','.inline-menu-title',function (e) {
    $(e.delegateTarget).toggleClass("inline-menu-expanded");
    $('header .menu-collapsed').toggleClass('reversed-color');
    $('.main-projects').toggleClass('show-content');
  });
  if ($('[reverse-main-menu]').length){
    // Add reverse-main-menu attribute to component to load the nav with the inverted colors
    $('header .menu-collapsed').addClass('reversed-color');
  }



  $('#film').click(function(){
    $('#film').addClass("selected");
    $('#photography').removeClass("selected");
    $('#retouching').removeClass("selected");
    $('#print').removeClass("selected");
    $('#development').removeClass("selected");

    $('.Film').css('display', 'block');

    $('.Photography').css('display', 'none');
    $('.Retouching').css('display', 'none');
    $('.Print').css('display', 'none');
    $('.Development').css('display', 'none');


  });

  $('#photography').click(function(){
    $('#film').removeClass("selected");
    $('#photography').addClass("selected");
    $('#retouching').removeClass("selected");
    $('#print').removeClass("selected");
    $('#development').removeClass("selected");

    $('.Film').css('display', 'none');

    $('.Photography').css('display', 'block');

    $('.Retouching').css('display', 'none');
    $('.Print').css('display', 'none');
    $('.Development').css('display', 'none');
  });

  $('#retouching').click(function(){
    $('#film').removeClass("selected");
    $('#photography').removeClass("selected");
    $('#retouching').addClass("selected");
    $('#print').removeClass("selected");
    $('#development').removeClass("selected");

    $('.Film').css('display', 'none');
    $('.Photography').css('display', 'none');

    $('.Retouching').css('display', 'block');

    $('.Print').css('display', 'none');
    $('.Development').css('display', 'none');
  });

  $('#print').click(function(){
    $('#film').removeClass("selected");
    $('#photography').removeClass("selected");
    $('#retouching').removeClass("selected");
    $('#print').addClass("selected");
    $('#development').removeClass("selected");

    $('.Film').css('display', 'none');
    $('.Photography').css('display', 'none');
    $('.Retouching').css('display', 'none');

    $('.Print').css('display', 'block');

    $('.Development').css('display', 'none');
  });

  $('#development').click(function(){
    $('#film').removeClass("selected");
    $('#photography').removeClass("selected");
    $('#retouching').removeClass("selected");
    $('#print').removeClass("selected");
    $('#development').addClass("selected");

    $('.Film').css('display', 'none');
    $('.Photography').css('display', 'none');
    $('.Retouching').css('display', 'none');
    $('.Print').css('display', 'none');

    $('.Development').css('display', 'block');
  });

  $('#all').click(function(){
    $('#film').removeClass("selected");
    $('#photography').removeClass("selected");
    $('#retouching').removeClass("selected");
    $('#print').removeClass("selected");
    $('#development').removeClass("selected");

    $('.Film').css('display', 'block');
    $('.Photography').css('display', 'block');
    $('.Retouching').css('display', 'block');
    $('.Print').css('display', 'block');
    $('.Development').css('display', 'block');
  });
})


// EDIT PROJECT SO IF THERE'S ONLY ONE CATEGORY IT DOESNT BREAK THE PAGE

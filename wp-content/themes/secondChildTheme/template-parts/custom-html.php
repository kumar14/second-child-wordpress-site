<?php if(get_row_layout() == 'custom_html'):?>
  <div class="custom-html" style="background-color:<?php the_sub_field('background_color') ?> ;">
    <?php the_sub_field('raw_html') ?>
  </div>
<?php endif; ?>

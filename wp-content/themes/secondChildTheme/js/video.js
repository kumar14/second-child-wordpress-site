jQuery(document).ready(function($) {
  let $videoPlayers = $('.video-player');

  // loop through potential players on page
  $videoPlayers.each(function(i){

    let player = this.querySelector('video');
    let customBtn = this.querySelector('.video-custom-button') || false;
    let playBtn = this.querySelector('.video-cta-play') || false;
    let $customBtn = $(customBtn);
    let $playBtn = $(playBtn);

    if (!player.hasAttribute('autoplay')) {
      // Standard play button enabled if video is not set to autoplay
      $playBtn.css('display', '');
      $customBtn.css('display','none');
    }
    // addEventLister with capture to prevent default video pausing event on video player
    // if video was already autoplaying silently when play button was clicked
    this.addEventListener('click',function(e){
      if(player && !player.getAttribute('controls')){
        e.preventDefault();
        if(player.hasAttribute('controls-on-click')){
          player.setAttribute('controls','true');
        }
        // Mute can still be toggled if controls are not present && !controls-on-click
        player.muted = !player.muted;
        player.play();
      }
        $customBtn.css('display', 'none');
        $playBtn.css('display','none');

    },true);
  });
})

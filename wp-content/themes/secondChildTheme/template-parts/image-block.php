<?php if(get_row_layout() == 'image_block'):?>
  <div class="image-block" style="background-color:<?php the_sub_field('background_color'); ?>; padding-top: <?php the_sub_field('padding_top'); ?>px; padding-bottom: <?php the_sub_field('padding_bottom'); ?>px;">
    <img class="image-block-image" src="<?php the_sub_field('image'); ?>" alt="" class="<?php the_sub_field('size'); ?>">
    <p class="image-block-caption"><?php the_sub_field('caption'); ?></p>
  </div>
<?php endif; ?>

<?php
  /*
    Plugin Name: SecondChild - Post Types
    Plugin URI:
    Description: Adds a new post type into Wordpress
    Version: 1.0
    Author: SecondChild
    Author URI: https://secondchild.nyc
    Text Domain: SecondChild
  */

  if(!defined('ABSPATH')) die();


// Register new Custom Post Type
function secondchild_class_post_type() {

	$labels = array(
		'name'                  => _x( 'Projects', 'Post Type General Name', 'secondchild' ),
		'singular_name'         => _x( 'Project', 'Post Type Singular Name', 'secondchild' ),
		'menu_name'             => __( 'Projects', 'secondchild' ),
		'name_admin_bar'        => __( 'Projects', 'secondchild' ),
		'archives'              => __( 'Archive', 'secondchild' ),
		'attributes'            => __( 'Attributes', 'secondchild' ),
		'parent_item_colon'     => __( 'Parent Item Colon', 'secondchild' ),
		'all_items'             => __( 'All Projects', 'secondchild' ),
		'add_new_item'          => __( 'Add Project', 'secondchild' ),
		'add_new'               => __( 'Add new', 'secondchild' ),
		'new_item'              => __( 'New Project', 'secondchild' ),
		'edit_item'             => __( 'Edit Project', 'secondchild' ),
		'update_item'           => __( 'Update Project', 'secondchild' ),
		'view_item'             => __( 'View Project', 'secondchild' ),
		'view_items'            => __( 'View Project', 'secondchild' ),
		'search_items'          => __( 'Search Project', 'secondchild' ),
		'not_found'             => __( 'Not found', 'secondchild' ),
		'not_found_in_trash'    => __( 'Not found in trash', 'secondchild' ),
		'featured_image'        => __( 'Featured Image', 'secondchild' ),
		'set_featured_image'    => __( 'Save Featured Image', 'secondchild' ),
		'remove_featured_image' => __( 'Remove Featured Image', 'secondchild' ),
		'use_featured_image'    => __( 'Use as Featured Image', 'secondchild' ),
		'insert_into_item'      => __( 'Insert in Project', 'secondchild' ),
		'uploaded_to_this_item' => __( 'Add in Project', 'secondchild' ),
		'items_list'            => __( 'Projects List', 'secondchild' ),
		'items_list_navigation' => __( 'Navigate to Projects', 'secondchild' ),
		'filter_items_list'     => __( 'Filter Projects', 'secondchild' ),
	);
	$args = array(
		'label'                 => __( 'Project', 'secondchild' ),
		'description'           => __( 'Projects for SecondChild Website', 'secondchild' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'hierarchical'          => false, // False = posts - No child posts
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
    'menu_position'         => 4,
    'menu_icon'             => 'dashicons-welcome-learn-more',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page'
    // 'show_in_rest'          => true
	);
	register_post_type( 'work', $args );
}
add_action( 'init', 'secondchild_class_post_type', 0);
?>

<?php if(get_row_layout() == 'page_background_color'):?>
  <body style="background-color:<?php the_sub_field('background_color') ?>; color:<?php the_sub_field('text_color') ?> ;">
  </body>
<?php endif; ?>

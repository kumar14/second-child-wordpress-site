<body class="careers-page" >
<?php get_header();  ?>

<img class="cp-img" src="<?php the_field('fullscreen_image'); ?>" alt="">


<div class="cp-content">
  <div class="cp-content-left">
    <p class="cp-title"><?php the_title(); ?></p>
    <p class="cp-blurb"><?php the_field('careers_blurb') ?></p>
  </div>

  <div class="cp-content-right">
    <?php if(have_rows('position_info')): ?>
      <?php while(have_rows('position_info')): the_row(); ?>
        <div class="position-content">
          <a class="pc-link" href="<?php the_sub_field('position_link'); ?>">
            <p class="pc-title"><?php the_sub_field('position_title'); ?></p>
          </a>
          <p class="pc-blurb"><?php the_sub_field('position_blurb'); ?></p>
        </div>
      <?php endwhile ?>
    <?php endif ?>
  </div>
</div>






</body>

<?php get_footer(); ?>

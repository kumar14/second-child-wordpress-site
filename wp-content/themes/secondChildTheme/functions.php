<?php

  show_admin_bar(false);

  //Link to the queries file
  require get_template_directory() . '/inc/queries.php';

  // Creates the Menus
  function secondchild_menu() {
    //Wordpress function
    register_nav_menus( array(
      'main-menu' => 'Main Menu',
      'social-menu' => 'Social Menu',
      'work-menu' => 'Work Menu',
      'footer-menu' => 'Footer Menu'
    ));
  }
  //Hook
  add_action('init', 'secondchild_menu');

  //Add Stylesheet and Javascriptfiles
  function secondchild_scripts() {
    //Normalize css
    wp_enqueue_style(
      'normalize',
      get_template_directory_uri() . '/css/normalize.css',
      array(),
      '8.0.1'
    );

    wp_enqueue_script('jquery');

    //Nav Stylesheet
    wp_enqueue_style(
      'collapsible-nav',
      get_template_directory_uri() . '/css/collapsible-nav.scss',
      array(),
      '1.0.0'
    );

    // Main Stylesheet
    // wp_enqueue_style(
    //   'main',
    //   get_template_directory_uri() . '/css/main.scss',
    //   array(),
    //   '1.0.0'
    // );

    //Google font
    wp_enqueue_style(
      'googlefont',
      'https://fonts.googleapis.com/css?family=Lora:400,700|Open+Sans:400,700&display=swap',
      array(),
      '1.0.0'
    );

    //Main Stylesheet
    wp_enqueue_style(
      'style',
      get_stylesheet_uri(),
      array('normalize', 'googlefont', 'collapsible-nav'),
      '1.0.0'
    );



    wp_enqueue_script(
      'custom-script',
      get_template_directory_uri() . '/js/nav.js',
      array('jquery')
    );

    wp_enqueue_script(
      'custom-script',
      get_template_directory_uri() . '/js/work-filter.js',
      array('jquery')
    );

    wp_enqueue_script(
      'video-player',
      get_template_directory_uri() . '/js/video.js',
      array('jquery')
    );
  }

  add_action('wp_enqueue_scripts', 'secondchild_scripts');

  //Enable feature images and other Stuff
  function secondchild_setup(){
    //register image size
    add_image_size( 'square', 350, 350, true);
    add_image_size( 'portrait', 350, 724, true);
    add_image_size( 'box', 400, 350, true);
    add_image_size( 'medium', 350, 700, true);
    add_image_size( 'blog', 966, 644, true);


    //add feeatured image
    add_theme_support('post-thumbnails');
  }

  add_action('after_setup_theme', 'secondchild_setup'); //when the theme is activated and ready

  // Create widget zone

  function secondchild_widgets() {
    register_sidebar( array(
        'name' => 'Sidebar',
        'id' => 'sidebar',
        'before_widget' => '<div class="widget">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));
  }
  add_action('widgets_init', 'secondchild_widgets');

  // displays the Hero image on background of the frontpage

  function secondchild_hero_image() {
    $front_page_id = get_option('page_on_front');
    $image_id = get_field('hero_image', $front_page_id);

    $image = $image_id['url'];

    // Create a "FALSE stylesheet"
    wp_register_style('custom', false);
    wp_enqueue_style('custom');

    $featured_image_css = "
      // .site-header-front{
      //   background-image: url( $image );
      // }
    ";

    wp_add_inline_Style('custom', $featured_image_css);
  }
  add_action('init', 'secondchild_hero_image');

  // Inline SVG icons
  function inline_svg_logo($svg_css = 'logo-white') {
    $svg = '<svg class="second-child-logo-svg '.$svg_css.'" version="1.1" id="_x32_C" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="35" height="52" x="0px" y="0px" viewBox="0 0 35 52" xml:space="preserve"><style type="text/css">.base-path{fill:#FFA4DF;}.top-path{fill:#383DD6;}</style><path class="top-path" d="M17.5,0C7.8,0,0,7.9,0,17.6h9.2c0-4.6,3.7-8.3,8.3-8.3s8.3,3.7,8.3,8.3s-3.7,8.3-8.3,8.3 c-0.8,0-1.6-0.1-2.4-0.4l-4.7,8.1c2.2,1,4.6,1.5,7.1,1.5c9.7,0,17.5-7.9,17.5-17.6C35,7.9,27.2,0,17.5,0"/><polygon class="base-path" points="5.3,42.7 0,52 35,52 35,42.7 "/></svg>';
    return $svg;
  }
  add_action('init', 'inline_svg_logo');

  function inline_instagram_logo($svg_css = 'icon-white') {
    $svg = '<svg class="instagram-icon svg-icon '.$svg_css.'" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="100%" height="100%" viewBox="0 0 169.063 169.063" xml:space="preserve"><g><path d="M122.406,0H46.654C20.929,0,0,20.93,0,46.655v75.752c0,25.726,20.929,46.655,46.654,46.655h75.752 c25.727,0,46.656-20.93,46.656-46.655V46.655C169.063,20.93,148.133,0,122.406,0z M154.063,122.407 c0,17.455-14.201,31.655-31.656,31.655H46.654C29.2,154.063,15,139.862,15,122.407V46.655C15,29.201,29.2,15,46.654,15h75.752 c17.455,0,31.656,14.201,31.656,31.655V122.407z"></path><path d="M84.531,40.97c-24.021,0-43.563,19.542-43.563,43.563c0,24.02,19.542,43.561,43.563,43.561s43.563-19.541,43.563-43.561 C128.094,60.512,108.552,40.97,84.531,40.97z M84.531,113.093c-15.749,0-28.563-12.812-28.563-28.561 c0-15.75,12.813-28.563,28.563-28.563s28.563,12.813,28.563,28.563C113.094,100.281,100.28,113.093,84.531,113.093z"></path><path d="M129.921,28.251c-2.89,0-5.729,1.17-7.77,3.22c-2.051,2.04-3.23,4.88-3.23,7.78c0,2.891,1.18,5.73,3.23,7.78 c2.04,2.04,4.88,3.22,7.77,3.22c2.9,0,5.73-1.18,7.78-3.22c2.05-2.05,3.22-4.89,3.22-7.78c0-2.9-1.17-5.74-3.22-7.78 C135.661,29.421,132.821,28.251,129.921,28.251z"></path></g></svg>';
    return $svg;
  }
  add_action('init', 'inline_instagram_logo');

  function inline_linkedin_logo($svg_css = 'icon-white') {
    $svg = '<svg class="linkedin-icon svg-icon '.$svg_css.'" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="100%" height="100%" viewBox="0 0 430.117 430.117" xml:space="preserve"><g><path id="LinkedIn" d="M430.117,261.543V420.56h-92.188V272.193c0-37.271-13.334-62.707-46.703-62.707 c-25.473,0-40.632,17.142-47.301,33.724c-2.432,5.928-3.058,14.179-3.058,22.477V420.56h-92.219c0,0,1.242-251.285,0-277.32h92.21 v39.309c-0.187,0.294-0.43,0.611-0.606,0.896h0.606v-0.896c12.251-18.869,34.13-45.824,83.102-45.824 C384.633,136.724,430.117,176.361,430.117,261.543z M52.183,9.558C20.635,9.558,0,30.251,0,57.463 c0,26.619,20.038,47.94,50.959,47.94h0.616c32.159,0,52.159-21.317,52.159-47.94C103.128,30.251,83.734,9.558,52.183,9.558z M5.477,420.56h92.184v-277.32H5.477V420.56z"></path></g></svg>';
    return $svg;
  }
  add_action('init', 'inline_instagram_logo');

?>

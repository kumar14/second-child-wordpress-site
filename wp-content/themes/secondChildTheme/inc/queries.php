<?php

function secondchild_projects_list() { ?>
  <div class="main-projects">
    <?php
      $args = array(
        'post_type' => 'work'

      );
      //use WP_Query and appecnd the results to classes
      $projects = new WP_Query($args);

      while($projects->have_posts()): $projects->the_post();
      // get_template_part('template-parts/project-card-block');
    ?>
    <div class="main-project-card <?php the_field('project_tags'); ?>" >
      <a class="card-wrapper-cta" href="<?php the_permalink(); ?>">
         <?php the_post_thumbnail()?>

         <div class="card-content">
          <div class="inner-content">
            <h5 class="client-name"><?php the_field('client_name'); ?></h5>
            <h3><?php the_title(); ?></h3>
          </div>
          <?php
            $tag_list = get_field('project_tags');
            $tag_list = str_replace(', ',',',$tag_list);
            $tags = explode(',',$tag_list);
            echo '<ul class="project-tags">';
            foreach ( $tags as $tag ) :
              echo "<li>$tag</li>";
            endforeach;
            echo '</ul>';
          ?>
         </div>
      </a>
    </div>
  <?php endwhile; wp_reset_postdata(); ?>
  </div>
<?php }





function secondchild_people_list() { ?>
  <ul class="main-people">
    <?php
      $args = array(
        'post_type' => 'people'
      );
      //use WP_Query and appecnd the results to classes
      $people = new WP_Query($args);
      while($people->have_posts()): $people->the_post();
    ?>

    <li class="main-people-card">
      <div class="card-content">
        <img src="<?php the_field('photo') ?>" alt="">
        <p class="personName"><?php the_field('first_name'); ?> <?php the_field('last_name'); ?></p>
        <p class="personTitle"><?php the_field('job_title'); ?></p>
      </div>
    </li>
  <?php endwhile; wp_reset_postdata(); ?>
  </ul>
<?php }

?>

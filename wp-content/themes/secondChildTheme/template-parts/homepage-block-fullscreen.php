<?php if(get_row_layout() == 'homepage_block_fullscreen'):?>
  <div class="homepage-block-fullscreen" style="background-color:<?php the_sub_field('background_color') ?> ;">
    <img class="hbf-image" src="<?php the_sub_field('main_image') ?>" alt="">

    <div class="hbf-content">
      <h2><?php the_sub_field('title') ?></h2>
      <h3><?php the_sub_field('subtitle') ?></h3>
      <div class="hbf-button">
        <a href="<?php the_sub_field('button_url') ?>"><?php the_sub_field('button_text') ?></a>
      </div>
    </div>
  </div>

<?php endif; ?>

<?php if(get_row_layout() == 'three_image_collage'): ?>
  <div class="three-image-collage">
    <?php if(get_sub_field('reverse_layout') == 'Yes'):?>
      <div class="tic-portrait">
        <img src="<?php the_sub_field('portrait_image') ?>" alt="">
      </div>

      <div class="tic-stacked">
        <img src="<?php the_sub_field('stacked_image_1') ?>" alt="">
        <img class="tic-stacked-2" src="<?php the_sub_field('stacked_image_2') ?>" alt="">
      </div>
    <?php else:?>
      <div class="tic-stacked">
        <img src="<?php the_sub_field('stacked_image_1') ?>" alt="">
        <img class="tic-stacked-2" src="<?php the_sub_field('stacked_image_2') ?>" alt="">
      </div>

      <div class="tic-portrait">
        <img src="<?php the_sub_field('portrait_image') ?>" alt="">
      </div>
    <?php endif ?>
  </div>
  <p class="tic-caption"><?php the_sub_field('caption') ?></p>
<?php endif ?>

<?php if(get_row_layout() == 'logo_grid'):?>
  <h3 class="logo-grid-headline"><?php the_sub_field('headline') ?></h3>
  <div class="logo-grid">


    <?php while(have_rows('logos')): the_row(); ?>
      <div class="logo-container">
        <a href="<?php the_sub_field('link') ?>">
          <img src="<?php the_sub_field('logo') ?>" />
        </a>
      </div>


    <?php endwhile ?>
  </div>
<?php endif; ?>

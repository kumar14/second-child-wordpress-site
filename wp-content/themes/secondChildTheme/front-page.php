<?php get_header('front') ?>

  <!-- <?php while(have_posts()): the_post(); ?>
    <section class="welcome text-center section">
      <h2 class="text-primary"><?php the_field('welcome_heading') ?></h2>
      <p class="text-primary-paragraph"><?php the_field('welcome_text') ?></p>
    </section> -->

    <!-- YOU CAN USE THIS SECTION ON THE ABOUT PAGE TO SHOW PEOPLE, BUT YOU NEED TO FIGURE OUT HOW TO MAKE IT MORE DYNAMIC
        BASICALLY USING A FOR LOOP FOR THE BLOCKS-->
    <!-- <section class="section-areas">
      <ul class="areas-container">
        <li class="area">
          <?php
            $workArea1 = get_field('work_area_1');
            $image1 = wp_get_attachment_image_src($workArea1['work_image'], 'medium')[0];
          ?>
          <img src="<?php echo $image1 ?>"/>
        </li>

        <li class="area">
          <?php
            $workArea2 = get_field('work_area_2');
            $image2 = wp_get_attachment_image_src($workArea2['work_image'], 'medium')[0];
          ?>
          <img src="<?php echo $image2 ?>"/>
        </li>

        <li class="area">
          <?php
            $workArea3 = get_field('work_area_3');
            $image3 = wp_get_attachment_image_src($workArea3['work_image'], 'medium')[0];
          ?>
          <img src="<?php echo $image3 ?>"/>
        </li>

        <li class="area">
          <?php
            $workArea4 = get_field('work_area_4');
            $image4 = wp_get_attachment_image_src($workArea4['work_image'], 'medium')[0];
          ?>
          <img src="<?php echo $image4 ?>"/>
        </li>
      </ul>
    </section> -->

    <!-- THIS SECTION IS BASICALLY SHOWING PROJECTS -->
    <!-- <section class="projects-homepage">
      <div class="container section">
        <h2 class="text-primary text-center">Our projects</h2>

        <?php secondchild_projects_list(); ?>

        <div class="button-container">
          <a href="<?php echo get_permalink( get_page_by_title('Work') ); ?>">See our projects</a>
        </div>
      </div>
    </section> -->


  <?php endwhile; ?>

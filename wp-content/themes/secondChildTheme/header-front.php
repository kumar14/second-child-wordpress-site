<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <?php wp_head(); ?>
    <title></title>

  </head>
<body <?php body_class(); ?>>
<?php echo do_shortcode('[smartslider3 slider="2"]');?>
  <header class="site-header-front">
    <div class="container header-grid">
      <div class="navigation-bar">

        <div class="menu-collapsed">
          <div class="bar">
            <span></span>
          </div>
          <!-- NAVIGATION MENU -->
          <!-- <img class="nav-logo" src='<?php echo get_template_directory_uri() . "/img/logo.svg"?>' alt="site-logo"> -->
          <div class="menu-content">
            <?php
              $args = array(
                'theme_location' => 'main-menu',
                'container' => 'nav',
                'container_class' => 'main-menu'
              );
              wp_nav_menu($args);
            ?>
            <div class="main-menu-info">
              <div class="menu-info-block">
                <h3>Location</h3>
                <span>120 Wall Street, Floor 2</span>
                <span>New York, NY 10005</span>
              </div>
              <div class="menu-info-block">
                <h3>General Inquiries</h3>
                <span>hello@secondchild.nyc</span>
                <span>917-237-8828</span>
              </div>
            </div>
          </div>
        </div>
        <!-- NAVIGATION LOGO -->
        <div class="logo">
          <a href="<?php echo home_url(); ?>">
            <?php echo inline_svg_logo(); ?>
          </a>
        </div>


      </div>
    </div>
  </header>
